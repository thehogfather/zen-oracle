Zen Protocol Oracle Example 
============

An oracle service example.  
This example uses [Intrinio](http://www.intrinio.com/) as data source.  

# Prerequisites

- [Mono](http://www.mono-project.com/) installed
- a running [MongoDB](https://www.mongodb.com/) service
- a running [Node](https://gitlab.com/zenprotocol/zenprotocol)

# Build

```
./paket restore
msbuild src
```

# Configuration

The `config.yaml` file contains the following configurable items:

- `nodeApi`: API address of the Zen Node used by the oracle service
- `api`: Incoming HTTP requests endpoint
- `tickers`: A list of tickers to fetch from Intrinio and commit in the blockchain
- `acs`: Active Contract Set settings:
    - `activate`: When activating the contract, this is the amount of blocks to have it activated for
    - `threshold`: If the contract is found to be active for less blocks than specified here, 'extend' will be triggered
    - `extend`: The amount of blocks to extend contract activation for
- `contract`: Oracle code file name
- `chain`: Specifies the chain (main, test, local)
- `derivationPath`: Specifies the HD derivation path used to perform authenticated contract execution

# Wallet Setup

Set up a wallet by importing a seed:

```
zen-cli import <ZEN-WALLET-PASSWORD> feel muffin volcano click mercy abuse bachelor ginger limb tomorrow okay input spend athlete boring security document exclude liar dune usage camera ranch thought
zen-cli resync
```

Lock some funds (Zen) to it. This will be used to activate and extend the oracle contract.

# Contract Setup

1. Source
    Refer to [examples](https://github.com/zenprotocol/contracts) repository for Oracle contract source code. Make the source file available in the filesystem.
    
2. Authentication
    As the oracle contract incorporates authenticated execution, a public key needs to be extracted from the node's wallet and embedded into the body (code) of the contract.
    Use the following command to extract a public key:

    ```
    zen-cli publickey "m/44'/258'/0'/3/0" <ZEN-WALLET-PASSWORD>
    ```
    
    > The `m/44'/258'/0'/3/0` [path](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki) is used by default by the oracle to perform an authenticated contract execution transaction.
    
    Then, in the source file, in line `let oraclePubKey = ""`, embed the result.
    
    For example:
    
    ```
    // The public key of the oracle  
    let oraclePubKey = "02add2eb8158d81b7ea51e35ddde9c8a95e24449bdab8391f40c5517bdb9a3e117"
    ```

3. Packing
    Using [Zebra, Zen SDK tool](https://github.com/zenprotocol/ZFS-SDK), pack the updated contract, for example:
    
    ```
    zebra --pack Oracle.fst
    ```
    
    Update `config.yaml` to refer to the generated, packed contract, for example:

    ```
    contract: Zf57a7bf099b4a1d13a22c0ff5a3706297d7ca74f9aca6cc78222175030bbf997.fst
    ```
    
# Running

Set the following environment variable and launch `zen-oracle.exe`

```
export intrinio_user_name=<INTRINIO-USERNAME>
export intrinio_password=<INTRINIO-PASSWORD>
export zen_account_password=<ZEN-WALLET-PASSWORD>
```