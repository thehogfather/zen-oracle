﻿module Main

open Consensus
open Serialization
open FsBech32
open Json
open Util
open Contract
open Config
open DataAccess
open Serialization
open Timestamp

[<EntryPoint>]
let main _ =
    result {
        let closingTime = currentClosingTime()
        
        do! Contract.ensureActive()

        let! data = Intrinio.fetch()
          
        let hashes =
            data
            |> Array.map (Merkle.hashLeaf closingTime)
            |> Array.toList

        do! hashes
            |> MerkleTree.computeRoot
            |> Hash.bytes
            |> Zen.Types.Data.data.Hash
            |> Data.serialize
            |> Base16.encode
            |> contractExecuteRequestJson contractId "Add" false config.yaml.derivationPath Array.empty
            |> execute "wallet/contract/execute"

        data
        |> Array.mapi (fun index (ticker, price) -> 
            {
                ticker = ticker
                price = price
                leaf = Merkle.hashLeaf closingTime (ticker, price)
                path = MerkleTree.createAuditPath hashes index |> List.toArray
            }
        )
        |> DataAccess.insert closingTime
    }
    |> function
    | Error e ->
        eprintfn "%s" e
        1
    | Ok _ ->
        printfn "data successfully persisted. %d record(s) preset" (DataAccess.count())
        0
