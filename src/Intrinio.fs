module Intrinio

open System
open FSharp.Data
open Util
open Json
open Config
open Infrastructure.Result

let basicAuthHeader = 
    sprintf "%s:%s" (getEnv "intrinio_user_name") (getEnv "intrinio_password")
    |> Serialization.String.serialize
    |> Convert.ToBase64String

let fetch() =
    getResponseBody (fun _ -> 
        Http.Request(
            "https://api.intrinio.com/data_point",
            httpMethod = "GET",
            query   = [ 
                "identifier", String.Join(",", config.yaml.tickers)
                "item", "close_price"
            ],
            headers = [ 
                "Authorization", basicAuthHeader 
            ]
        )
    ) "error getting provider data"
    >>= parseRawResultJson
