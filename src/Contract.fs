module Contract

open System
open System.IO
open Consensus
open Infrastructure
open Result
open FSharp.Data
open Util
open Json
open Config

let private contracCode = 
    Exception.resultWrap<String> (fun _ -> 
        (Platform.workingDirectory, config.yaml.contract)
        |> Path.Combine
        |> File.ReadAllText
    ) "error reading contract file"
    
let contractId = 
    contracCode
    <@> Contract.makeContractId Types.Version0
    |> get
    
let private activate() = result { 
    let! code = contracCode

    let! activatedContractId =
        getResponseBody (fun _ ->
            "wallet/contract/activate"
            |> getUri
            |> (contractActivateRequestJson code config.yaml.acs.activate).Request 
        ) "error activating contract"
        >>= parseContractActivateResponseJson

    if activatedContractId <> contractId then
        return! Error "error activating contract"
    else
        printfn "contract activated"
}

let private extend() = result { 
    let address = 
        contractId
        |> Wallet.Address.Contract
        |> Wallet.Address.encode chain
        
    do! getResponseBody (fun _ ->
            "wallet/contract/extend"
            |> getUri
            |> (contractExtendRequestJson address config.yaml.acs.extend).Request 
        ) "error extending contract"
        <@> fun _ -> printfn "contract extended"
}

let private getTip() = 
    getResponseBody (fun _ ->
        Http.Request(
            "blockchain/info"
            |> getUri,
            httpMethod = "GET"
        )) "error getting blockchain info"
    >>= parseBlockChainInfoJson
    <@> fun info -> info.Blocks

let ensureActive() = result {
    let! activationState =
        getResponseBody (fun _ ->
            Http.Request(
                getUri "contract/active",
                httpMethod = "GET"
            ))
            "trying to get ACS, got:"
        >>= parseActiveContractsResponseJson
        <@> List.tryFind (fst >> (=) contractId) 
        <@> Option.map snd
    
    match activationState with
    | None -> 
        printfn "contract is not active - activating..."
        do! activate()
    | Some activationState ->
        let! tip = getTip()
         
        if activationState - tip < config.yaml.acs.threshold then
            printfn "contract active for %d blocks, extending..." (activationState - tip)
            do! extend()
}

let execute action json =
    getResponseBody (fun _ ->
        getUri action
        |> (json:JsonValue).Request
    ) "error executing contract"
    <@> fun _ -> printfn "contract executed"