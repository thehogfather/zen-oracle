module Timestamp

open System
open Infrastructure.Timestamp
open Config

[<Literal>]
let private DateFormat = "yyyy-MM-dd"
let private epoch = (new DateTime(1970, 1, 1, 0, 0, 0, 0))

let private est = TimeZoneInfo.FindSystemTimeZoneById("EST")

//TODO: handle daylight saving time. use ET instead of EST
let closingTime (date : DateTime) =
    let date = DateTime.SpecifyKind(date.Date.AddHours(config.yaml.closingHour), DateTimeKind.Unspecified)
    TimeZoneInfo.ConvertTimeToUtc(date, est)
    |> Infrastructure.Timestamp.fromDateTime

let currentClosingTime() =
    DateTime.Now.Date
    |> closingTime
    
let parse date =
    try
        DateTime.ParseExact(date, DateFormat, Globalization.CultureInfo.InvariantCulture)
        |> closingTime
        |> Some
    with _ ->
        None

let toString (timestamp: Timestamp) =
    (epoch.AddMilliseconds (timestamp |> float)).ToString(DateFormat)

let normalize (timestamp: Timestamp) =
    timestamp / 1000UL