module DataAccess

open MongoDB.Bson
open MongoDB.Driver
open MongoDB.FSharp
open Infrastructure.Timestamp
open Consensus.Hash

[<Literal>]
let ConnectionString = "mongodb://localhost"

[<Literal>]
let DbName = "oracle"

[<Literal>]
let CollectionName = "data"

type Item = {
    ticker: string
    price: decimal
    leaf: Hash
    path: Hash[]
}

type Data = { 
    id: BsonObjectId
    timestamp: Timestamp
    data: Item array
} 

let client = MongoClient(ConnectionString)
let db = client.GetDatabase(DbName)
let collection = db.GetCollection<Data>(CollectionName)

let insert timestamp data =
   {
        id = MongoDB.Bson.BsonObjectId(MongoDB.Bson.ObjectId.GenerateNewId())
        timestamp = timestamp
        data = data
   }
   |> collection.InsertOne

let list() =
    collection.Find(fun _ -> true).ToEnumerable()

let take count =
    list()
    |> Seq.rev
    |> Seq.truncate count
    |> Seq.rev

let count = 
    list >> Seq.length

let find (timestamp:Timestamp) = 
    list()
    |> Seq.tryFind (fun x -> x.timestamp = timestamp)